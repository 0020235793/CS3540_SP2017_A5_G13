#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "Game.h"
#include "dieNdice.h"
#include "ScoreCard.h"

using namespace std;

TEST_CASE("Test core functionality", "Classes") {

	die aDie1, aDie2, aDie3, aDie4, aDie5;

	SECTION("Testing Die class") {
		REQUIRE(aDie1.getVal(aDie1) == 0);
		REQUIRE(aDie2.getVal(aDie2) == 0);
		REQUIRE(aDie3.getVal(aDie3) == 0);
		REQUIRE(aDie4.getVal(aDie4) == 0);
		REQUIRE(aDie5.getVal(aDie5) == 0);
	}
	
	dice allDie;
	allDie.rollDice(aDie1, aDie2, aDie3, aDie4, aDie5);
	SECTION("Testing Dice class #1") {
		REQUIRE(aDie1.getVal(aDie1) > 0);
		REQUIRE(aDie2.getVal(aDie2) > 0);
		REQUIRE(aDie3.getVal(aDie3) > 0);
		REQUIRE(aDie4.getVal(aDie4) > 0);
		REQUIRE(aDie5.getVal(aDie5) > 0);
	}

	allDie.rollDice(aDie1, aDie2, aDie3, aDie4, aDie5);
	SECTION("Testing Dice class #2") {
		REQUIRE(aDie1.getVal(aDie1) > 0);
		REQUIRE(aDie2.getVal(aDie2) > 0);
		REQUIRE(aDie3.getVal(aDie3) > 0);
		REQUIRE(aDie4.getVal(aDie4) > 0);
		REQUIRE(aDie5.getVal(aDie5) > 0);
	}

	ScoreCard score;
	int D[5];

	D[0] = aDie1.getVal(aDie1);
	D[1] = aDie2.getVal(aDie2);
	D[2] = aDie3.getVal(aDie3);
	D[3] = aDie4.getVal(aDie4);
	D[4] = aDie5.getVal(aDie5);
	
    SECTION("Testing Values of all die through Array") {
        REQUIRE(D[0] >= 0);
        REQUIRE(D[1] >= 0);
        REQUIRE(D[2] >= 0);
        REQUIRE(D[3] >= 0);
        REQUIRE(D[4] >= 0);
    }

	SECTION("Testing ScoreCard class") {
		REQUIRE(score.Calc0(D) >= 0);
		REQUIRE(score.Calc1(D) >= 0);
		REQUIRE(score.Calc2(D) >= 0);
		REQUIRE(score.Calc3(D) >= 0);
		REQUIRE(score.Calc4(D) >= 0);
		REQUIRE(score.Calc5(D) >= 0);
		REQUIRE(score.Calc6(D) >= 0);
		REQUIRE(score.Calc7(D) >= 0);
		REQUIRE(score.Calc8(D) >= 0);
		REQUIRE(score.Calc9(D) >= 0);
		REQUIRE(score.Calc10(D) >= 0);
		REQUIRE(score.Calc11(D) >= 0);
		REQUIRE(score.Calc12(D) >= 0);
		REQUIRE(score.Calc13() >= 0);
	}

	SECTION("Testing Game class") {
		REQUIRE(score.Calc0(D) >= 0);
		REQUIRE(score.Calc1(D) >= 0);
		REQUIRE(score.Calc2(D) >= 0);
		REQUIRE(score.Calc3(D) >= 0);
		REQUIRE(score.Calc4(D) >= 0);
		REQUIRE(score.Calc5(D) >= 0);
		REQUIRE(score.Calc6(D) >= 0);
		REQUIRE(score.Calc7(D) >= 0);
		REQUIRE(score.Calc8(D) >= 0);
		REQUIRE(score.Calc9(D) >= 0);
		REQUIRE(score.Calc10(D) >= 0);
		REQUIRE(score.Calc11(D) >= 0);
		REQUIRE(score.Calc12(D) >= 0);
		REQUIRE(score.Calc13() >= 0);
	}
}