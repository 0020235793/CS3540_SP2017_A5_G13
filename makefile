CC = g++
CFLAGS = -Wall
DEPS = Game.h dieNdice.h
INCLUDE = -I ../include
INCLUDE_TESTS = -I ../tests
TARGET = fib.exe
OBJ = Main.o 

all: INCLUDE = -I../include
all: TARGET = fib.exe
all: SRC = ../src/main.cpp ../include/Fib.cpp

tests: INCLUDE = -I../test -I../include
tests: TARGET = fibTest.exe
tests: SRC = ../tests/Test.cpp

Test: Test.o Game.o dieNdice.o 
	$(CC) $(SRC) $(INCLUDE) -o $(TARGET)
Test.o: Test.cpp
	$(CC) -c Test.cpp -I ../tests
Main: Main.o Game.o dieNdice.o Test.o
	$(CC) Main.o Game.o dieNdice.o -g -o Main
Main.o: Main.cpp Game.cpp
	$(CC) $(CFLAGS) -c Main.cpp
Game.o: Game.cpp Game.h dieNdice.cpp
	$(CC) $(CFLAGS) -c Game.cpp
dieNdice.o: dieNdice.cpp dieNdice.h
	$(CC) $(CFLAGS) -c dieNdice.cpp
clean:
	rm -f *.o Main Game dieNdice Test
 
#	$(CC) $(CFLAGS) -c Main.cpp
#-I../include -o fib.exe (name output file fib)
#all:
#	g++ Main.cpp -I../include -o fib.exe (name output file fib)
#tests:
#	g++ Test.cpp -I../include -o fibtests.exe
#				$(INCLUDE_TESTS) -o $(TARGET)
#clean:
#rm *.exe *.o