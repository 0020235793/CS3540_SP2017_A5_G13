#include "dieNdice.h"
#include "math.h"
#include <iomanip>
#include <iostream>
#include <stdlib.h>

using namespace std;

die::die()
{
	faceValue = 0;
}
void die::roll(die &aDie)
{
	aDie.faceValue = rand() % 6 + 1;
}
void die::displayAllDie(die &die1, die &die2, die &die3, die &die4, die &die5)
{
	cout << "The dice have the current value" << endl;
	cout << die1.faceValue << " " << die2.faceValue << " " << die3.faceValue << " " << die4.faceValue << " " << die5.faceValue << endl;
}
int die::getVal(die &aDie)
{
	return aDie.faceValue;
}
void dice::rollDice(die &die1, die &die2, die &die3, die &die4, die &die5)
{
	die1.roll(die1);
	die2.roll(die2);
	die3.roll(die3);
	die4.roll(die4);
	die5.roll(die5);
	displayAllDie(die1, die2, die3, die4, die5);
}
