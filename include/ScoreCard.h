/*							File name: ScoreCard.h
	This file calculates, displays, and stores score information for the yahtzee player.



   Variable = Catagorie
-------------------------------
	Card[0] = Ones
	Card[1] = Twos
	Card[2] = Threes
	Card[3] = Fours
	Card[4] = Fives
	Card[5] = Sixes
	Card[6] = Three of a Kind
	Card[7] = Four of a Kind
	Card[8] = Full House
	Card[9] = Small Straight
	Card[10] = Large Straight
	Card[11] = Chance
	Card[12] = Yahtzee
	Card[13] = Total
*/	

#ifndef SCORECARD_H
#define SCORECARD_H
#include<iostream>
#include<string>
#include<iomanip>
#include "dieNdice.h"

using namespace std;

class ScoreCard {
private:
	int Card[14];
	int Disp[14];
	int D[5];
	bool Open[14];

public:
	ScoreCard()
	{
		for (int i = 0; i < 14; i++)
		{
			Disp[i] = 0;
			Card[i] = 0;
			Open[i] = true;
		}
	}
	
	void Display(int Die1, int Die2, int Die3, int Die4, int Die5)
	{
		int Choice = 0;

		D[0] = Die1;
		D[1] = Die2;
		D[2] = Die3;
		D[3] = Die4;
		D[4] = Die5;

		for(int i = 0; i < 13; i++)
		{
			Disp[i]=0;
		}

		Disp[0] = Calc0(D);
		Disp[1] = Calc1(D);
		Disp[2] = Calc2(D);
		Disp[3] = Calc3(D);
		Disp[4] = Calc4(D);
		Disp[5] = Calc5(D);
		Disp[6] = Calc6(D);
		Disp[7] = Calc7(D);
		Disp[8] = Calc8(D);
		Disp[9] = Calc9(D);
		Disp[10] = Calc10(D);
		Disp[11] = Calc11(D);
		Disp[12] = Calc12(D);

		if(Open[0] == true)
			cout << setw(20) << "1. Ones" << " | " << setw (2) << Disp[0] << " |" << endl;
		if(Open[1] == true)
			cout << setw(20) << "2. Twos" << " | " << setw(2) << Disp[1] << " |" << endl;
		if (Open[2] == true)
			cout << setw(20) << "3. Threes" << " | " << setw(2) << Disp[2] << " |" << endl;
		if (Open[3] == true)
			cout << setw(20) << "4. Fours" << " | " << setw(2) << Disp[3] << " |" << endl;
		if (Open[4] == true)
			cout << setw(20) << "5. Fives" << " | " << setw(2) << Disp[4] << " |" << endl;
		if (Open[5] == true)
			cout << setw(20) << "6. Sixes" << " | " << setw(2) << Disp[5] << " |" << endl;
		if (Open[6] == true)
			cout << setw(20) << "7. Three of a kind" << " | " << setw(2) << Disp[6] << " |" << endl;
		if (Open[7] == true)
			cout << setw(20) << "8. Four of a kind" << " | " << setw(2) << Disp[7] << " |" << endl;
		if (Open[8] == true)
			cout << setw(20) << "9. Full House" << " | " << setw(2) << Disp[8] << " |" << endl;
		if (Open[9] == true)
			cout << setw(20) << "10. Small Straight" << " | " << setw(2) << Disp[9] << " |" << endl;
		if (Open[10] == true)
			cout << setw(20) << "11. Large Straight" << " | " << setw(2) << Disp[10] << " |" << endl;
		if (Open[11] == true)
			cout << setw(20) << "12. Yahtzee" << " | " << setw(2) << Disp[11] << " |" << endl;
		if (Open[12] == true)
			cout << setw(20) << "13. Chance" << " | " << setw(2) << Disp[12] << " |" << endl;

		cout << "Please enter the score you would like to save (1 - 13): ";
		cin >> Choice;

		while ((Choice < 1 || Choice > 13) || Open[Choice - 1] != true)
		{
			cout << "Not a valid input. Please enter a value between 1 and 13: ";
			cin >> Choice;
		}

		SaveScore(Choice);

	}

	void SaveScore(int Choice)
	{
		int Loc = Choice - 1;
		
		for (int i = 0; i < 13; i++)
		{
			if (i == Loc)
			{
				Card[i] = Disp[i];
				Open[i] = false;
			}
		}

		Card[13] = Calc13();
	}

	int Calc0(int D[])
	{
		int cnt1s = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 1)
				cnt1s++;
		}

		return (1 * cnt1s);
	}

	int Calc1(int D[])
	{
		int cnt2s = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 2)
				cnt2s++;
		}

		return (cnt2s * 2);
	}

	int Calc2(int D[])
	{
		int cnt3s = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 3)
				cnt3s++;
		}

		return (cnt3s * 3);
	}

	int Calc3(int D[])
	{
		int cnt4s = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 4)
				cnt4s++;
		}

		return (cnt4s * 4);
	}

	int Calc4(int D[])
	{
		int cnt5s = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 5)
				cnt5s++;
		}

		return (cnt5s * 5);
	}

	int Calc5(int D[])
	{
		int cnt6s = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 6)
				cnt6s++;
		}

		return (cnt6s * 6);
	}

	int Calc6(int D[])
	{
		int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, score = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 1)
				cnt1++;
			else if (D[i] == 2)
				cnt2++;
			else if (D[i] == 3)
				cnt3++;
			else if (D[i] == 4)
				cnt4++;
			else if (D[i] == 5)
				cnt5++;
			else if (D[i] == 6)
				cnt6++;
		}

		if (cnt1 >= 3 || cnt2 >= 3 || cnt3 >= 3 || cnt4 >= 3 || cnt5 >= 3 || cnt6 >= 3)
			score = D[0] + D[1] + D[2] + D[3] + D[4];

		return score;
	}

	int Calc7(int D[])
	{
		int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, score = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 1)
				cnt1++;
			else if (D[i] == 2)
				cnt2++;
			else if (D[i] == 3)
				cnt3++;
			else if (D[i] == 4)
				cnt4++;
			else if (D[i] == 5)
				cnt5++;
			else if (D[i] == 6)
				cnt6++;
		}

		if (cnt1 >= 4 || cnt2 >= 4 || cnt3 >= 4 || cnt4 >= 4 || cnt5 >= 4 || cnt6 >= 4)
			score = D[0] + D[1] + D[2] + D[3] + D[4];

		return score;
	}

	int Calc8(int D[])
	{
		int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, score = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 1)
				cnt1++;
			else if (D[i] == 2)
				cnt2++;
			else if (D[i] == 3)
				cnt3++;
			else if (D[i] == 4)
				cnt4++;
			else if (D[i] == 5)
				cnt5++;
			else if (D[i] == 6)
				cnt6++;
		}

		if((cnt1 == 3 || cnt2 == 3 || cnt3 == 3 || cnt4 == 3 || cnt5 == 3 || cnt6 == 3) && (cnt1 == 2 || cnt2 == 2 || cnt3 == 2 || cnt4 == 2 || cnt5 == 2 || cnt6 == 2))
			score = 25;

		return score;
	}

	int Calc9(int D[])
	{
		int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, score = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 1)
				cnt1++;
			else if (D[i] == 2)
				cnt2++;
			else if (D[i] == 3)
				cnt3++;
			else if (D[i] == 4)
				cnt4++;
			else if (D[i] == 5)
				cnt5++;
			else if (D[i] == 6)
				cnt6++;
		}

		if (cnt1 >= 1 && cnt2 >= 1 && cnt3 >= 1 && cnt4 >= 1)
			score = 30;
		else if (cnt2 >= 1 && cnt3 >= 1 && cnt4 >= 1 && cnt5 >= 1)
			score = 30;
		else if (cnt3 >= 1 && cnt4 >= 1 && cnt5 >= 1 && cnt6 >= 1)
			score = 30;

		return score;
	}

	int Calc10(int D[])
	{
		int cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, cnt6 = 0, score = 0;

		for (int i = 0; i < 5; i++)
		{
			if (D[i] == 1)
				cnt1++;
			else if (D[i] == 2)
				cnt2++;
			else if (D[i] == 3)
				cnt3++;
			else if (D[i] == 4)
				cnt4++;
			else if (D[i] == 5)
				cnt5++;
			else if (D[i] == 6)
				cnt6++;
		}

		if (cnt1 >= 1 && cnt2 >= 1 && cnt3 >= 1 && cnt4 >= 1 && cnt5 >= 1)
			score = 40;
		else if (cnt2 >= 1 && cnt3 >= 1 && cnt4 >= 1 && cnt5 >= 1 && cnt6 >= 1)
			score = 40;

		return score;
	}

	int Calc11(int D[])
	{
		int score = 0;

		if (D[0] == D[1] && D[1] == D[2] && D[2] == D[3] && D[3] == D[4])
			score = 50;

		return score;
	}

	int Calc12(int D[])
	{
		int score = 0;

		for (int i = 0; i < 5; i++)
			score += D[i];

		return score;
	}

	int Calc13()
	{
		int total = 0;

		for (int i = 0; i < 13; i++)
			total += Card[i];

		return total;
	}
	void Total()
	{
		cout << "Final Score" << endl;
		for(int i = 0; i < 13; i++){
			cout << Card[i] << endl;
		}
		cout << "Total Score" << endl << Card[13] << endl;
	}


};


#endif

