#include "Game.h"
#include "ScoreCard.h"
#include <iomanip>
#include <iostream>
#include <stdlib.h>

using namespace std;


void Game :: Play()
{
	int  track [13];
	ScoreCard score;

	for(int i = 0; i < 13; i++)
	{
	

		die die1, die2, die3, die4, die5;

		dice allDie;

		allDie.rollDice(die1, die2, die3, die4, die5);

		int a [5] = {0,0,0,0,0};
		a[0] = die1.getVal(die1);
		a[1] = die2.getVal(die2);
		a[2] = die3.getVal(die3);
		a[3] = die4.getVal(die4);
		a[4] = die5.getVal(die5);

		score.Display(a[0],a[1],a[2],a[3],a[4]);

	}
	score.Total();
}
void Game :: Display(ScoreCard score, int track [])
{

	// int input = 0;
	// score.Display();
	// do{
	// 	cout << "select what value you would like recorded (1-13): ";
	// 	cin >> input;
	// }while(CheckEntry(track, input))

	// score.Record(input);
	// score.Display();

}
bool Game :: Restart()
{
	string input;

	cout << "Play again? (y | n)";
	cin >> input;

	if(input == "y")
	{
		replay = true;
	}
	else
	{
		replay = false;
	}
	return replay;
}
bool Game :: CheckEntry(int track [], int a)
{
	bool found = false;
	for(int i = 0; i < 13; i++)
	{
		if(track[i] == a)
		{
			cout << "Invalid Entry" << endl;
			found = true;
		}
	}
	return found;
}