//Authors: Barich, ALex : Irick, Michael : Trumble, Kevin : Reed, Ryan
//Description: This is a game of single player yahtzee tracked through version control
//Date: 3-16-17

#include <iomanip>
#include <string>
#include <iostream>
#include "Game.h"

using namespace std;

int main(){

	Game game;

	while(game.Restart())
	{
		game.Play();
	}


	return 0;

}